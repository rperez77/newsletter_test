<?php

use App\model\Newsletters;

require('vendor/autoload.php');

class NewsletterTest extends \PHPUnit_Framework_TestCase
{

  protected $client;

  public static function setUpBeforeClass() {
    Newsletters::truncate();
  }

  public static function tearDownAfterClass() {
    Newsletters::truncate();
  }

  protected function setUp() {
    $this->client = new GuzzleHttp\Client(['base_uri' => 'http://localhost:1349']);
  }

  public function testAddNewsletter() {
    $arrayDataNewsletter = ['name' => 'Newsletter','type' => 'Standard', 'subject'=>'Hello', 'body'=>'Hello'];
    $response = $this->client->request('POST', 'index.php?c=newsletter&a=save', 
      [
        'form_params' => $arrayDataNewsletter
      ]
    );
    $this->assertEquals($response->getStatusCode(), 200);
    $newsletters = Newsletters::all();
    $this->assertEquals(1,count($newsletters));
    $this->assertEquals($newsletters[0]->name,$arrayDataNewsletter['name']);
    $this->assertEquals($newsletters[0]->type,$arrayDataNewsletter['type']);
    $this->assertEquals($newsletters[0]->subject,$arrayDataNewsletter['subject']);
    $this->assertEquals($newsletters[0]->body,$arrayDataNewsletter['body']);
    return $newsletters[0]->id;;
  }

  /**
   * @depends testAddNewsletter
   */
  public function testEditNewsletter($id_newsletter) {
    $arrayDataNewsletter = ['id' => $id_newsletter, 'name' => 'Newsletter New','type' => 'Welcome', 'subject'=>'Hi', 'body'=>'Hi'];
    $response = $this->client->request('POST', 'index.php?c=newsletter&a=save', 
      [
        'form_params' => $arrayDataNewsletter
      ]
    );
    $this->assertEquals($response->getStatusCode(), 200);
    $newsletters = Newsletters::all();
    $this->assertEquals(1,count($newsletters));
    $this->assertEquals($newsletters[0]->name,$arrayDataNewsletter['name']);
    $this->assertEquals($newsletters[0]->type,$arrayDataNewsletter['type']);
    $this->assertEquals($newsletters[0]->subject,$arrayDataNewsletter['subject']);
    $this->assertEquals($newsletters[0]->body,$arrayDataNewsletter['body']);
  }

  /**
   * @depends testAddNewsletter
   */
  public function testDeleteNewsletter($id_newsletter) {
    $response = $this->client->request('GET', 'index.php?c=newsletter&a=delete&id='.$id_newsletter);
    $this->assertEquals($response->getStatusCode(), 200);
    $newsletters = Newsletters::all();
    $this->assertEquals(0,count($newsletters));
  }

  
}
