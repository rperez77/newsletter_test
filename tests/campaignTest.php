<?php

require('vendor/autoload.php');

use App\model\Newsletters;
use App\model\Campaigns;
use App\model\Lists;
use App\model\Suscribers;
use App\lib\Database;

require_once "modelTest/campaigns_list.php";
require_once "modelTest/suscribrers_lists.php";

class CampaignTest extends \PHPUnit_Framework_TestCase
{

  protected $client,$db,$msqli;

  public static function setUpBeforeClass() {
    Newsletters::truncate();
    Campaigns::truncate();
    Suscribers::truncate();
    Lists::truncate();
    CampaignsLists::truncate();
    SuscribersLists::truncate();

  }

  public static function tearDownAfterClass() {
    Suscribers::truncate();
    Lists::truncate();
    Newsletters::truncate();
    Campaigns::truncate();
    CampaignsLists::truncate();
    SuscribersLists::truncate();
  }

  protected function setUp() {
    $this->client = new GuzzleHttp\Client(['base_uri' => 'http://localhost:1349','cookies'=>true]);
  }

  public function testAddCampaign() {
    $suscriber = new Suscribers();
    $suscriber->name = "Roger";
    $suscriber->email = "roger.a.guerra@gmail.com";
    $suscriber->save();
    $list = new Lists();
    $list->name = "The list";
    $list->save();
    $list->addSuscriber($suscriber);
    $newsletter = new Newsletters();
    $newsletter->name = "Newsletter";
    $newsletter->subject = "Hello";
    $newsletter->type = "Standard";
    $newsletter->body = "Hello";
    $newsletter->save();
    $date = date('Y-m-d H:i:s');
    $lists = array($list->id);
    $jar = new \GuzzleHttp\Cookie\CookieJar;
    $token = $this->client->request('GET', 'index.php?c=campaign&a=setToken',['cookies' => $jar])->getBody()->getContents();
    $arrayDataCampaign = ['name' => 'Campaign','send_at' => $date, 'send_now'=>'on', 'lists'=>$lists, 'newsletter'=> $newsletter->id, 'token'=>$token];
    $response = $this->client->request('POST', 'index.php?c=campaign&a=save', 
      [
        'form_params' => $arrayDataCampaign,
        'cookies' => $jar
      ]
    );
    $this->assertEquals($response->getStatusCode(), 200);
    $campaigns = Campaigns::all();
    $this->assertEquals(1,count($campaigns));
    $this->assertEquals($campaigns[0]->name,$arrayDataCampaign['name']);
    $this->assertEquals($campaigns[0]->send_at,$arrayDataCampaign['send_at']);
  }

  

  
}
