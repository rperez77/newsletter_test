<?php

use App\model\Suscribers;
use App\model\Lists;

require('vendor/autoload.php');

class SuscriberTest extends \PHPUnit_Framework_TestCase
{

  protected $client;

  public static function setUpBeforeClass() {
    Suscribers::truncate();
  }

  public static function tearDownAfterClass() {
    Suscribers::truncate();
  }

  protected function setUp() {
    $this->client = new GuzzleHttp\Client(['base_uri' => 'http://localhost:1349']);
  }

  public function testAddSuscriber() {
    $arrayDataSuscriber = ['name' => 'Roger', 'email' => 'roger.a.guerra@gmail.com'];
    $response = $this->client->request('POST', 'index.php?c=suscriber&a=save', 
      [
        'form_params' => $arrayDataSuscriber
      ]
    );
    $this->assertEquals($response->getStatusCode(), 200);
    $suscribers = Suscribers::all();
    $this->assertEquals(1,count($suscribers));
    $this->assertEquals($suscribers[0]->name,$arrayDataSuscriber['name']);
    $this->assertEquals($suscribers[0]->email,$arrayDataSuscriber['email']);
    return $suscribers[0]->id;;
  }

  /**
   * @depends testAddSuscriber
   */
  public function testEditSuscriber($id_suscriber) {
    $arrayNewDataSuscriber = ['id'=>$id_suscriber, 'name' => 'Nataly', 'email' => 'nataly@gmail.com'];
    $response = $this->client->request('POST', 'index.php?c=suscriber&a=save', 
      [
        'form_params' => $arrayNewDataSuscriber
      ]
    );
    $this->assertEquals($response->getStatusCode(), 200);
    $suscribers = Suscribers::all();
    $this->assertEquals(1,count($suscribers));
    $this->assertEquals($suscribers[0]->name,$arrayNewDataSuscriber['name']);
    $this->assertEquals($suscribers[0]->email,$arrayNewDataSuscriber['email']);
   
  }

  /**
   * @depends testAddSuscriber
   */
  public function testDeleteSuscriber($id_suscriber) {
    $response = $this->client->request('GET', 'index.php?c=suscriber&a=delete&id='.$id_suscriber);
    $this->assertEquals($response->getStatusCode(), 200);
    $suscribers = Suscribers::all();
    $this->assertEquals(0,count($suscribers));
  }
}
