<?php

use App\model\Suscribers;
use App\model\Lists;

require('vendor/autoload.php');

class ListTest extends \PHPUnit_Framework_TestCase
{

  protected $client;

  public static function setUpBeforeClass() {
    Suscribers::truncate();
    Lists::truncate();
    SuscribersLists::truncate();
  }

  public static function tearDownAfterClass() {
    Suscribers::truncate();
    Lists::truncate();
    SuscribersLists::truncate();
  }

  protected function setUp() {
    $this->client = new GuzzleHttp\Client(['base_uri' => 'http://localhost:1349']);
  }

  public function testAddList() {
    $arrayDataList = ['name' => 'The list'];
    $response = $this->client->request('POST', 'index.php?c=list&a=save', 
      [
        'form_params' => $arrayDataList
      ]
    );
    $this->assertEquals($response->getStatusCode(), 200);
    $lists = Lists::all();
    $this->assertEquals(1,count($lists));
    $this->assertEquals($lists[0]->name,$arrayDataList['name']);
    return $lists[0]->id;;
  }

  /**
   * @depends testAddList
   */
  public function testEditList($id_list) {
    $arrayNewDataList = ['id'=>$id_list, 'name' => 'New list'];
    $response = $this->client->request('POST', 'index.php?c=list&a=save', 
      [
        'form_params' => $arrayNewDataList
      ]
    );
    $this->assertEquals($response->getStatusCode(), 200);
    $lists = Lists::all();
    $this->assertEquals(1,count($lists));
    $this->assertEquals($lists[0]->name,$arrayNewDataList['name']);
  }

  /**
   * @depends testAddList
   */
  public function testAddSuscriberToList($id_list) {
    $suscriber = new Suscribers();
    $suscriber->name = "Roger";
    $suscriber->email = "roger.a.guerra@gmail.com";
    $suscriber->save();
    $suscribers = array($suscriber->id);
    $arrayDataList = ['id'=>$id_list, 'suscribers[]' => $suscribers];
    $response = $this->client->request('POST', 'index.php?c=list&a=postAddSuscriber', 
      [
        'form_params' => $arrayDataList
      ]
    );
    $this->assertEquals($response->getStatusCode(), 200);
    $list = Lists::find($id_list);
    $suscribers_list = $list->suscribers();
    $this->assertInternalType('array', $suscribers_list);
    $this->assertCount(1,$suscribers_list);
    $this->assertEquals($suscriber->id,$suscribers_list[0]->id);
  }


  /**
   * @depends testAddList
   */
  public function testDeleteList($id_list) {
    $response = $this->client->request('GET', 'index.php?c=list&a=delete&id='.$id_list);
    $this->assertEquals($response->getStatusCode(), 200);
    $lists = Lists::all();
    $this->assertEquals(0,count($lists));
  }
}
