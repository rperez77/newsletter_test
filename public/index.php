<?php

require __DIR__.'/../vendor/autoload.php';

$baseController = 'App\controller\\';

$controller = 'Suscriber';

// Todo esta lógica hara el papel de un FrontController
if(!isset($_REQUEST['c']))
{

    $controllerName = $baseController.$controller.'Controller';
    $controller = new $controllerName();
    $controller->index();
}
else
{
    // Obtenemos el controlador que queremos cargar
    $controller = strtolower($_REQUEST['c']);
    $accion = isset($_REQUEST['a']) ? $_REQUEST['a'] : 'index';

    // Instanciamos el controlador
    $controllerName = $baseController.ucwords($controller) . 'Controller';
    $controller = new $controllerName();

    // Llama la accion
    call_user_func( array( $controller, $accion ) );
}






?>
