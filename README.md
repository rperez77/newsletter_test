# Newsletter System

## Roger Perez Solution

#### Requirements

* Web server (Apache or Nginx)
* PHP >=5.5.3
* Mysql >=5.6.19 
 
#### Walkthrough

To run the app:

* Create db newsletter
* Import newsletter.sql to db
* Change access crediantals to database in app/lib/Database.php
* Into to the main directory of the project, run this command:
```php composer.phar install```

To execute the tests:

* Into to the main directory of the project, run this command:
```
./vendor/bin/phpunit
```







