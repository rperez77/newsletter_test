<?php

namespace App\model;

require __DIR__.'/../../vendor/autoload.php';

use App\lib\Database;

class Model  {


  static $db;
  static $mysqli;

  public function __construct() {
    self::startDB();
  }
  //Function to start db connection using Database library
  private static function startDB() {
    try {
      self::$db = Database::getInstance();
      self::$mysqli = self::$db->getConnection();

    } catch (Exception $e) {
      var_dump($e);
    }
  }

  //Function to get a record by id
  public static function find($id) {
    $class = get_called_class();
    $instance = new $class();
    $query = "SELECT * FROM ".$instance->table." WHERE id = ?";
    $stmt = self::$mysqli->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result = $stmt->get_result();
    if( $result->num_rows>0 ){
      $row = $result->fetch_object();
      foreach ( $row as $property => $value ) {
        $instance->$property = $value;
      }
      return $instance;
    } else {
      return $instance;
    }
  }

  //Function to get all records
  public static function all() {
    self::startDB();
    $class = get_called_class();
    $varsClass = get_class_vars($class );
    $query = "SELECT * FROM ".$varsClass["table"];
    $result = self::$mysqli->query($query);
    $objects = array();
    if( $result ){
      while( $row = $result->fetch_object() ){
        $objects[] = $row;
      }
    }
    return $objects;
  }

  //Function to get records with where clause
  public static function where($field,$value) {
    self::startDB();
    $class = get_called_class();
    $varsClass = get_class_vars($class );
    $query = "SELECT * FROM ".$varsClass["table"]." WHERE ".$field." = ?";
    $type = gettype($value);
    switch ($type) {
      case 'string':
        $bind_type= "s";
        break;

      case 'integer':
        $bind_type= "i";
        break;

      case 'double':
        $bind_type= "d";
        break;
      
      default:
        # code...
        break;
    }
    $stmt = self::$mysqli->prepare($query);
    $stmt->bind_param($bind_type, $value);
    $stmt->execute();
    $result = $stmt->get_result();
    $objects = array();
    if( $result ){
      while( $row = $result->fetch_object() ){
        $objects[] = $row;
      }
    }
    return $objects;
  }

  //Function to delete a record by id
  public function delete() {

    if( isset($this->id) ) {
      $query = "DELETE FROM ".$this->table." WHERE id = ".$this->id;
      $result = self::$mysqli->query($query);
      if( $result ){
        unset($this);
        return $result;
      } else {
        trigger_error("Failed to connect to MySQL: " . mysqli_error(self::$mysqli),
         E_USER_ERROR);
      }
    }
  }
  //Function to insert or edit a record
  public function save() {
    $set="";
    if( isset($this->id) ) {
      foreach($this as $key => $v) {
          if( $key!="table" ) {
            $val = is_numeric($v) ? $v : "'" . $v . "'";
            $set .= sprintf("%s=%s%s", $key, $val, ($v == end($this) ? "" : ", "));
          }
      }
      $query = sprintf("UPDATE %s SET %s WHERE id = %d", $this->table, $set,$this->id);
      $result = self::$mysqli->query($query);
      if( $result ) {
        return $result;
      }
    } else {
      $fields = $values = array();
      foreach( array_keys(get_object_vars($this)) as $key ) {
          if( !in_array($key, array("table")) ) {
              $fields[] = "`$key`";
              $values[] = "'" . self::$mysqli->real_escape_string($this->$key) . "'";
          }
      }
      $fields = implode(",", $fields);
      $values = implode(",", $values);
      $query = "INSERT INTO ".$this->table." ($fields) VALUES ($values)";
      $result = self::$mysqli->query($query);
      if( $result ) {
        $this->id= self::$mysqli->insert_id;
        return $result;
      } else {
        trigger_error("Failed to connect to MySQL: " . mysqli_error(self::$mysqli),
         E_USER_ERROR);
      }
    }
  }

  //Function to insert records in a pivot table
  public function addRelationPivotTable($object,$tablePivot,$key,$other_key) {
    $query = sprintf("INSERT INTO %s (`%s`,`%s`) VALUES (%d, %d)",$tablePivot,$key,$other_key,$this->id,$object->id);
    $result = self::$mysqli->query($query);
    if( $result ){
      return $result;
    } else {
      trigger_error("Failed to connect to MySQL: " . mysqli_error(self::$mysqli),
       E_USER_ERROR);
    }
  }

  //Function to add a relation
  public function addRelation($object,$key,$other_key) {
    $query = sprintf("UPDATE %s SET %s=%d WHERE id = %d",$this->table,$key,$object->$other_key,$this->id);
    $result = self::$mysqli->query($query);
    if( $result ){
      return $result;
    } else {
      trigger_error("Failed to connect to MySQL: " . mysqli_error(self::$mysqli),
       E_USER_ERROR);
    }
  }

  //Function to get records related in pivot table
  public function getRelationPivotTable($table,$tablePivot, $key,$other_key) {
    $query = sprintf('SELECT %1$s.* FROM %1$s INNER JOIN %2$s ON %2$s.%3$s = %1$s.id WHERE %4$s = %5$d', $table, $tablePivot, $key,$other_key,$this->id );
    $result = self::$mysqli->query($query);
    $objects = array();
    if( $result ){
      while( $row = $result->fetch_object() ){
        $objects[] = $row;
      }
    }
    return $objects;
  }

  //Function to get record related in table
  public function getRelation($table, $key,$other_key) {
    $query = sprintf('SELECT %1$s.* FROM %1$s  WHERE %2$s = %3$d', $table, $key, $this->$other_key);
    $result = self::$mysqli->query($query);
    $objects = array();
    if( $result ){
      while( $row = $result->fetch_object() ){
        $objects[] = $row;
      }
    }
    return $objects;
  }

  //Function to remove records in a pivot table
  public function removeRelationPivotTable($object,$tablePivot,$key,$other_key) {
    $query = sprintf("DELETE FROM %s WHERE %s=%d AND %s=%d",$tablePivot,$key,$this->id,$other_key,$object->id);
    $result = self::$mysqli->query($query);
    if( $result ){
      return $result;
    } else {
      trigger_error("Failed to connect to MySQL: " . mysqli_error(self::$mysqli),
       E_USER_ERROR);
    }
  }

  public static function truncate() {
    self::startDB();
    $class = get_called_class();
    $varsClass = get_class_vars($class );
    self::$mysqli->query('SET foreign_key_checks = 0');
    $query = "TRUNCATE TABLE  ".$varsClass["table"];
    $result = self::$mysqli->query($query);
    self::$mysqli->query('SET foreign_key_checks = 1');
    return $result;
  }
}

?>
