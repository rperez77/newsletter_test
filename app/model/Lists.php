<?php

namespace App\model;

require __DIR__.'/../../vendor/autoload.php';

class Lists extends Model {

  protected $table = "lists";


  public function addSuscriber($suscriber) {
    return $this->addRelationPivotTable($suscriber,'suscribers_lists','id_list','id_suscriber');
  }

  public function suscribers() {
    return $this->getRelationPivotTable('suscribers','suscribers_lists','id_suscriber','id_list');
  }

  public function removeSuscriber($suscriber) {
    return $this->removeRelationPivotTable($suscriber,'suscribers_lists','id_list','id_suscriber');
  }


}

?>
