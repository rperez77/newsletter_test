<?php

namespace App\model;

require __DIR__.'/../../vendor/autoload.php';

class Campaigns extends Model {

  protected $table = "campaigns";


  public function addList($list) {
    return $this->addRelationPivotTable($list,'campaigns_lists','id_campaign','id_list');
  }

  public function lists() {
    return $this->getRelationPivotTable('lists','campaigns_lists','id_list','id_campaign');
  }

  public function newsletter() {
    return $this->getRelation('newsletters','id','id_newsletter');
  }

  public function attachNewsletter($newsletter) {
    return $this->addRelation($newsletter,'id_newsletter','id');
  }

}

?>