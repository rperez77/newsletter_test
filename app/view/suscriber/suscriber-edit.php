<h1 class="page-header">
  <?php echo isset($suscriber) ? $suscriber->name : 'New Suscriber'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=suscriber">Suscribers</a></li>
  <li class="active"><?php echo isset($suscriber)  ? $suscriber->name : 'New Suscriber'; ?></li>
</ol>

<form id="frm-suscriber" action="?c=suscriber&a=save" method="post" enctype="multipart/form-data">
  <input type="hidden" name="id" value="<?php echo isset($suscriber) ? $suscriber->id:"" ?>" />

  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" value="<?php echo isset($suscriber) ? $suscriber->name:"" ?>" class="form-control" placeholder="Name" data-validacion-tipo="requerido|min:3" />
  </div>

  <div class="form-group">
    <label>Email</label>
    <input type="email" name="email" value="<?php echo isset($suscriber) ? $suscriber->email:"" ?>" class="form-control" placeholder="Email" data-validacion-tipo="requerido|min:10" />
  </div>

  <hr />

  <div class="text-right">
    <button class="btn btn-success">Save</button>
  </div>

</form>

<script>
  $(document).ready(function(){
    $("#frm-suscriber").validate({
      rules: {
        name: {
          required: true,
          minlength: 5
        },
        email: {
          required: true,
          email: true
        }
      },
      messages: {
        name: {
          required: "Please provide a name",
          minlength: "Newsletter's name must be at least 5 characters long"
        },
        email: "Please enter a valid email address" 
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  })
</script>
