<h1 class="page-header">Suscribers</h1>

<div class="well well-sm text-right">
  <a class="btn btn-primary" href="?c=suscriber&a=edit">New Suscriber</a>
</div>

<table class="table table-striped">
  <thead>
    <tr>
      <th style="width:180px;">Name</th>
      <th>Email</th>
      <th style="width:60px;"></th>
      <th style="width:60px;"></th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($suscribers as $suscriber): ?>
  <tr>
    <td><?php echo $suscriber->name; ?></td>
    <td><?php echo $suscriber->email; ?></td>
    <td>
      <a href="?c=suscriber&a=edit&id=<?php echo $suscriber->id; ?>">Edit</a>
    </td>
    <td>
      <a onclick="javascript:return confirm('Are you sure you want to delete this?');" href="?c=suscriber&a=delete&id=<?php echo $suscriber->id; ?>">Delete</a>
    </td>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>
