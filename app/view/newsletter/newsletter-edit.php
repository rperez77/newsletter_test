<h1 class="page-header">
  <?php echo isset($newsletter) ? $newsletter->name : 'New Newsletter'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=newsletter">Newsletters</a></li>
  <li class="active"><?php echo isset($newsletter)  ? $newsletter->name : 'New Newsletter'; ?></li>
</ol>

<form id="frm-newsletter" action="?c=newsletter&a=save" method="post" enctype="multipart/form-data">
  <input type="hidden" name="id" value="<?php echo isset($newsletter) ? $newsletter->id:"" ?>" />

  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" value="<?php echo isset($newsletter) ? $newsletter->name:"" ?>" class="form-control" placeholder="Name" data-validacion-tipo="requerido|min:3" />
  </div>

  <div class="form-group">
    <label>Subject</label>
    <input type="text" name="subject" value="<?php echo isset($newsletter) ? $newsletter->subject:"" ?>" class="form-control" placeholder="Subject" data-validacion-tipo="requerido|min:5" />
  </div>

  <div class="form-group">
    <label>Type</label>
    <select class="form-control" name="type">
      <?php $types = array('Standard','Welcome','Notification' ); ?>
      <option value="">Select a type</option>
      <?php foreach($types as $type): ?>
        <option value="<?php echo $type; ?>" <?php echo (isset($newsletter) && $type==$newsletter->type)?'selected':'' ?>><?php echo $type; ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group">
    <label>Body</label>
    <textarea name="body" id="body" cols="30" rows="10"><?php echo isset($newsletter) ? $newsletter->body:"" ?></textarea>
  </div>

  <script>
    CKEDITOR.replace( 'body' );
   </script>

  <hr />

  <div class="text-right">
    <button class="btn btn-success">Save</button>
  </div>

</form>

<script>
  $(document).ready(function(){
    $("#frm-newsletter").validate({
      rules: {
        name: {
          required: true,
          minlength: 5
        },
        subject: {
          required: true,
          minlength: 5
        },
        type: "required",
        body: "required"
      },
      messages: {
        name: {
          required: "Please provide a name",
          minlength: "Newsletter's name must be at least 5 characters long"
        },
        subject: {
          required: "Please provide a subject",
          minlength: "Newsletter's subject must be at least 5 characters long"
        },
        type: "Please choose a type"
        
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  })
</script>
