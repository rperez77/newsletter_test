<h1 class="page-header">Newsletters</h1>

<div class="well well-sm text-right">
  <a class="btn btn-primary" href="?c=newsletter&a=edit">New Newsletter</a>
</div>

<table class="table table-striped">
  <thead>
      <tr>
        <th style="width:180px;">Name</th>
        <th>Subject</th>
        <th>Type</th>
        <th style="width:60px;"></th>
        <th style="width:60px;"></th>
      </tr>
  </thead>
  <tbody>
  <?php foreach($newsletters as $newsletter): ?>
    <tr>
      <td><?php echo $newsletter->name; ?></td>
      <td><?php echo $newsletter->subject; ?></td>
      <td><?php echo $newsletter->type; ?></td>
      <td>
        <a href="?c=newsletter&a=edit&id=<?php echo $newsletter->id; ?>">Edit</a>
      </td>
      <td>
        <a onclick="javascript:return confirm('Are you sure you want to delete this?');" href="?c=newsletter&a=delete&id=<?php echo $newsletter->id; ?>">Delete</a>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

