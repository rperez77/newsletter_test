<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Newsletter</title>
      <meta charset="utf-8" />
      <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
      <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css" />
      <link rel="stylesheet" href="assets/js/jquery-ui/jquery-ui.min.css" />
      <link rel="stylesheet" href="assets/css/style.css" />
      <link rel="stylesheet" href="assets/css/bootstrap-switch.min.css" >
      <link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css" >
      <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
      <script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
  </head>
  <body>
    <div class="container">
      <header>
        <ul class="nav nav-tabs">
          <li role="presentation" class="<?php echo (!isset($_REQUEST['c']) || $_REQUEST['c']=='suscriber')?'active':'' ?>"><a href="?c=suscriber">Suscribers</a></li>
          <li role="presentation" class="<?php echo (isset($_REQUEST['c']) && $_REQUEST['c']=='list')?'active':'' ?>"><a href="?c=list">Lists</a></li>
          <li role="presentation" class="<?php echo (isset($_REQUEST['c']) && $_REQUEST['c']=='newsletter')?'active':'' ?>"><a href="?c=newsletter">Newsletters</a></li>
          <li role="presentation" class="<?php echo (isset($_REQUEST['c']) && $_REQUEST['c']=='campaign')?'active':'' ?>"><a href="?c=campaign">Campaigns</a></li>
        </ul>
      </header>
    </div>
    <div class="container">
