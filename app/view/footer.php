      <div class="row">
        <div class="col-xs-12">
          <hr />
          <footer class="text-center well">
            <p>Code Challenge</p>
          </footer>
        </div>
      </div>
    </div>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/js/ini.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/bootstrap-switch.min.js"></script>
    <script src="assets/js/moment.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
  </body>
</html>
