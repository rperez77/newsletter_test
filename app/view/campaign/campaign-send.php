<h1 class="page-header">
  <?php echo isset($campaign) ? $campaign->name : 'New Campaign'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=campaign">Campaigns</a></li>
  <li class="active"><?php echo isset($campaign)  ? $campaign->name : 'New Campaign'; ?></li>
</ol>

<form id="frm-campaign" action="?c=campaign&a=save" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name"  class="form-control" placeholder="Name"  />
  </div>

  <div class="form-group">
    <label>Newsletter</label>
    <select class="form-control" name="newsletter" >
      <option value="">Select a newsletter</option>
      <?php foreach($newsletters as $newsletter): ?>
        <option value="<?php echo $newsletter->id; ?>"><?php echo $newsletter->name; ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group">
    <label>Send at:</label>
    <input type="checkbox" name="send_now" checked data-on-text="Now" data-off-text="Later">
    <div class="datetime-container">
      <input type="text" class="form-control" id='send_at' name="send_at" />
    </div>
  </div>

  <div class="form-group">
    <label>Lists</label>
    <select multiple class="form-control" name="lists[]">
      <?php foreach($lists as $list): ?>
        <option value="<?php echo $list->id; ?>"><?php echo $list->name; ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <hr />

  <input type="hidden" name="token" value="<?php echo $_SESSION['token'] ?>" />
  <div class="text-right">
      <button class="btn btn-success">Send</button>
  </div>

</form>

<script>
  $(document).ready(function(){
    $("#frm-campaign").validate({
      rules: {
        name: {
          required: true,
          minlength: 5
        },
        newsletter: "required",
        "lists[]": "required"
      },
      messages: {
        name: {
          required: "Please provide a name",
          minlength: "Campaign's name must be at least 5 characters long"
        },
        newsletter: "Please choose a newsletter",
        "lists[]": "Please choose at least one list"
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
    $("#send_at").val(moment().format(format));
    var format = "YYYY-MM-DD HH:mm:ss";
    $("[name='send_now']").bootstrapSwitch();
    $("input[name='send_now']").on("switchChange.bootstrapSwitch", function(event, state) {
      if( state==false ){
        $(".datetime-container").css("display","inline-block");
      } else {
        $("#send_at").val(moment().format(format));
        $(".datetime-container").css("display","none");
      }
    });
    $('#send_at').datetimepicker({
      format:format
    });
  })
</script>
