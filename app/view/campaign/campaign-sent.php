<h1 class="page-header">
  <?php echo isset($campaign) ? $campaign->name : 'New Campaign'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=campaign">Campaigns</a></li>
  <li class="active"><?php echo isset($campaign)  ? $campaign->name : 'New Campaign'; ?></li>
</ol>

<h2>Emails Output:</h2>

<?php foreach($suscribers_array as $suscriber): ?>
  <div class="well well-sm output-email">
    <span>Recipient: <?php echo $suscriber->name ?> &lt;<?php echo $suscriber->email ?>&gt;</span>
    <span>Subject: <?php echo $newsletter->subject ?></span>
    <?php echo $newsletter->body ?>
  </div>
<?php endforeach; ?>

