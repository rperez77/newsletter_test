<h1 class="page-header">Campaign</h1>

<div class="well well-sm text-right">
  <a class="btn btn-primary" href="?c=campaign&a=edit">New Campaign</a>
</div>

<table class="table table-striped">
  <thead>
    <tr>
      <th style="width:180px;">Name</th>
      <th>Send at</th>
      <th>Sent at</th>
      <th>Newsletter</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($campaigns as $campaign): ?>
    <tr>
      <td><?php echo $campaign->name; ?></td>
      <td><?php echo $campaign->send_at; ?></td>
      <td><?php echo $campaign->sent_at; ?></td>
      <td><?php echo $campaign->newsletter; ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

