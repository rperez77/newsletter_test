<h1 class="page-header">Lists</h1>

<div class="well well-sm text-right">
  <a class="btn btn-primary" href="?c=list&a=edit">New List</a>
</div>

<table class="table table-striped">
  <thead>
    <tr>
      <th style="width:180px;">Name</th>
      <th style="width:60px;"></th>
      <th style="width:60px;"></th>
      <th style="width:60px;"></th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($lists as $list): ?>
    <tr>
      <td><?php echo $list->name; ?></td>
      <td>
        <a href="?c=list&a=getAddSuscribers&id=<?php echo $list->id; ?>">Add Suscribers to List</a>
      </td>
      <td>
        <a href="?c=list&a=edit&id=<?php echo $list->id; ?>">Edit</a>
      </td>
      <td>
        <a onclick="javascript:return confirm('Are you sure you want to delete this?');" href="?c=list&a=delete&id=<?php echo $list->id; ?>">Delete</a>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
