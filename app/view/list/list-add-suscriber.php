<h1 class="page-header">
  <?php echo $list->name; ?> :: Add Suscribers
</h1>

<ol class="breadcrumb">
  <li><a href="?c=list">Lists</a></li>
  <li class="active"><?php echo  $list->name; ?></li>
</ol>

<form id="frm-list-add-suscriber" action="?c=list&a=postAddSuscriber" method="post" enctype="multipart/form-data">
  <input type="hidden" name="id" value="<?php echo $list->id; ?>" />
  <div class="row">
    <div class="col-md-6">
      <h2>Suscribers</h2>
      <select multiple class="form-control" name="suscribers[]">
        <?php foreach($suscribers as $suscriber): ?>
          <?php if (!in_array($suscriber,$suscribers_list)): ?>
            <option value="<?php echo $suscriber->id; ?>"><?php echo $suscriber->name; ?> - <?php echo $suscriber->email; ?> </option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="col-md-6">
      <h2>Suscribers in List</h2>
      <ul class="list-group">
        <?php foreach($suscribers_list as $suscriber): ?>
          <li class="list-group-item">Name:<?php echo $suscriber->name; ?><br>Email:<?php echo $suscriber->email; ?></li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>

  <hr />

  <div class="text-right">
    <button class="btn btn-success">Save</button>
  </div>
  
</form>

