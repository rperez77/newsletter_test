<h1 class="page-header">
    <?php echo isset($list) ? $list->name : 'New List'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=list">Lists</a></li>
  <li class="active"><?php echo isset($list)  ? $list->name : 'New List'; ?></li>
</ol>

<form id="frm-list" action="?c=list&a=save" method="post" enctype="multipart/form-data">
  <input type="hidden" name="id" value="<?php echo isset($list) ? $list->id:"" ?>" />

  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" value="<?php echo isset($list) ? $list->name:"" ?>" class="form-control" placeholder="Name" />
  </div>

  <div class="form-group">
    <select multiple class="form-control" name="suscribers[]">
      <?php foreach($suscribers as $suscriber): ?>
        <?php if (!in_array($suscriber,$suscribers_list)): ?>
          <option value="<?php echo $suscriber->id; ?>"><?php echo $suscriber->name; ?> - <?php echo $suscriber->email; ?> </option>
        <?php endif; ?>
      <?php endforeach; ?>
    </select>
  </div>  

  <hr />

  <div class="text-right">
    <button class="btn btn-success">Save</button>
  </div>
</form>

<script>
  $(document).ready(function(){
    $("#frm-list").validate({
      rules: {
        name: {
          required: true,
          minlength: 5
        }
      },
      messages: {
        name: {
          required: "Please provide a name",
          minlength: "Campaign's name must be at least 5 characters long"
        }
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  })
</script>
