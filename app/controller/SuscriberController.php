<?php

namespace App\controller;

require __DIR__.'/../../vendor/autoload.php';

use App\model\Suscribers;

class SuscriberController {

  public function index(){
    $suscribers = Suscribers::all();
    require_once __DIR__.'/../view/header.php';
    require_once __DIR__.'/../view/suscriber/suscriber.php';
    require_once __DIR__.'/../view/footer.php';
  }

  public function edit(){
    if(isset($_REQUEST['id'])){
        $suscriber = Suscribers::find($_REQUEST['id']);
    }
    require_once __DIR__.'/../view/header.php';
    require_once __DIR__.'/../view/suscriber/suscriber-edit.php';
    require_once __DIR__.'/../view/footer.php';
  }

  public function save(){
    if(isset($_REQUEST['id'])){
      $suscriber = Suscribers::find($_REQUEST['id']);
    } else {
      $suscriber = new Suscribers();
    }
    $suscriber->name = $_REQUEST['name'];
    $suscriber->email = $_REQUEST['email'];
    $suscriber->save();
    
    header('Location: index.php');
  }

  public function delete(){
    $suscriber = Suscribers::find($_REQUEST['id']);
    $suscriber->delete();
    header('Location: index.php');
  }
}
