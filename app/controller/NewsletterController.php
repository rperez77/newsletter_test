<?php

namespace App\controller;

require __DIR__.'/../../vendor/autoload.php';

use App\model\Newsletters;

class NewsletterController {

  public function index(){
    $newsletters = Newsletters::all();
    require_once __DIR__.'/../view/header.php';
    require_once __DIR__.'/../view/newsletter/newsletter.php';
    require_once __DIR__.'/../view/footer.php';
  }

  public function edit(){
    if(isset($_REQUEST['id'])){
        $newsletter = Newsletters::find($_REQUEST['id']);
    }
    require_once __DIR__.'/../view/header.php';
    require_once __DIR__.'/../view/newsletter/newsletter-edit.php';
    require_once __DIR__.'/../view/footer.php';
  }

  public function save(){
    if(isset($_REQUEST['id'])){
      $newsletter = Newsletters::find($_REQUEST['id']);
    } else {
      $newsletter = new Newsletters();
    }
    $newsletter->name = $_REQUEST['name'];
    $newsletter->subject = $_REQUEST['subject'];
    $newsletter->type = $_REQUEST['type'];
    $newsletter->body = $_REQUEST['body'];
    $newsletter->save();
    header('Location: index.php?c=newsletter');
  }

  public function delete(){
    $newsletter = Newsletters::find($_REQUEST['id']);
    $newsletter->delete();
    header('Location: index.php?c=newsletter');
  }
}
