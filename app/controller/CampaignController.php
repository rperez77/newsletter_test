<?php

namespace App\controller;

require __DIR__.'/../../vendor/autoload.php';

use App\model\Lists;
use App\model\Newsletters;
use App\model\Campaigns;
use App\model\Suscribers;

class CampaignController {

  public function index(){
    $campaigns = Campaigns::all();
    foreach ($campaigns as $campaign) {
      $campaign->newsletter = Newsletters::find($campaign->id_newsletter)->name;
    }
    require_once __DIR__.'/../view/header.php';
    require_once __DIR__.'/../view/campaign/campaign.php';
    require_once __DIR__.'/../view/footer.php';
  }

  public function edit(){
    session_start();
    $lists = Lists::all();
    $newsletters = Newsletters::where('type','Standard');
    if(isset($_REQUEST['id'])){
        $campaign = Campaigns::find($_REQUEST['id']);
    }
    $_SESSION['token'] = md5(session_id() . time());
    require_once __DIR__.'/../view/header.php';
    require_once __DIR__.'/../view/campaign/campaign-send.php';
    require_once __DIR__.'/../view/footer.php';
  }

  public function setToken(){
    session_start();
    $_SESSION['token'] = md5(session_id() . time());
    echo $_SESSION['token'];
  }

  public function save(){
    session_start();
    print_r($_REQUEST);
    if ( isset($_SESSION['token']) && isset($_REQUEST['token']) ) {
      if ( $_REQUEST['token'] != $_SESSION['token'] ){
        header('Location: index.php?c=campaign');
      } else {
        $suscribers_array = [];
        $lists = $_REQUEST['lists'];
        $campaign = new Campaigns();
        $campaign->name = $_REQUEST['name'];
        $campaign->send_at = $_REQUEST['send_at'];
        if(isset($_REQUEST['send_now']))
          $campaign->sent_at = $_REQUEST['send_at'];
        $newsletter = Newsletters::find($_REQUEST['newsletter']);
        $campaign->save();
        $campaign->attachNewsletter($newsletter);
        foreach ( $lists as $list ) {
          $list = Lists::find($list);
          $campaign->addList($list);
          $suscribers = $list->suscribers();
          foreach ($suscribers as $suscriber) {
            if ( !in_array($suscriber,$suscribers_array) ) {
              array_push($suscribers_array,$suscriber);
            } 
            //mail("someone@example.com","My subject",$msg);
          }
        }
        $_SESSION['token'] = md5(session_id() . time());
        require_once __DIR__.'/../view/header.php';
        require_once __DIR__.'/../view/campaign/campaign-sent.php';
        require_once __DIR__.'/../view/footer.php';
      }
    }  
  }

}
