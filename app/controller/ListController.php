<?php

namespace App\controller;

require __DIR__.'/../../vendor/autoload.php';

use App\model\Suscribers;
use App\model\Lists;

class ListController {

  public function index(){
    $lists = Lists::all();
    require_once __DIR__.'/../view/header.php';
    require_once __DIR__.'/../view/list/list.php';
    require_once __DIR__.'/../view/footer.php';
  }

  public function edit(){
    $suscribers_list = [];
    $suscribers = Suscribers::all();
    if(isset($_REQUEST['id'])){
      $list = Lists::find($_REQUEST['id']);
      $suscribers_list = $list->suscribers();
    }
    require_once __DIR__.'/../view/header.php';
    require_once __DIR__.'/../view/list/list-edit.php';
    require_once __DIR__.'/../view/footer.php';
  }

  public function save(){
    if(isset($_REQUEST['id'])){
      $list = Lists::find($_REQUEST['id']);
    } else {
      $list = new Lists();
    }
    $list->name = $_REQUEST['name'];
    $list->save();
    $suscribers = $_REQUEST['suscribers'];
    foreach ($suscribers as $suscriber ) {
      $suscriber = Suscribers::find($suscriber);
      $list->addSuscriber($suscriber);
    }
    header('Location: index.php?c=list');
  }

  public function getAddSuscribers(){
    $list = Lists::find($_REQUEST['id']);
    $suscribers_list = $list->suscribers();
    $suscribers = Suscribers::all();
    require_once __DIR__.'/../view/header.php';
    require_once __DIR__.'/../view/list/list-add-suscriber.php';
    require_once __DIR__.'/../view/footer.php';
  }

  public function postAddSuscriber(){
    $list = Lists::find($_REQUEST['id']); 
    $suscribers = $_REQUEST['suscribers'];
    foreach ($suscribers as $suscriber ) {
      $suscriber = Suscribers::find($suscriber);
      $list->addSuscriber($suscriber);
    }
    header('Location: index.php?c=list');
  }

  public function delete(){
    $list= Lists::find($_REQUEST['id']);
    $suscribers_list = $list->suscribers();
    foreach ($suscribers_list as $suscriber) {
      $list->removeSuscriber($suscriber);
    }
    $list->delete();
    header('Location: index.php?c=list');
  }
}
